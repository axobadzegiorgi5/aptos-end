import { useState } from "react";
import "./verificationpage.css"
import ReactInputVerificationCode from 'react-input-verification-code';
import Countdown from 'react-countdown';

const VerificationPage = () => {

    const [emailValue, setEmailValue] = useState("")
    const [errorMessage, setErrorMessage] = useState(false)
    const [emailSuccess, setEmailSuccess] = useState(false)
    const [codeSuccess, setCodeSuccess] = useState(false)
    const emailDomain = emailValue.split("@")[1]

    function validateEmail() {
        setErrorMessage(true)
            if ((emailValue.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/g) != null) && 
                emailDomain == "aptos.com" || emailDomain == "aptos.global"|| emailDomain == "aptos.cz") {
                    setErrorMessage(false)
                    setEmailSuccess(true)

                    return true;
                }    
    }

    const renderer = ({minutes, seconds}) => {
          return <span>{minutes}:{seconds}</span>;
      };


    return(
            <div className="email-verification">
                <div className="email-verification__header">
                Upload files
                </div>
                <div className="email-verification__inputs">
                    <div className="email-verification__title">{emailSuccess ? "Write a code, you recieved on e-mail" : "Write your e-mail for verification and recieve a code"}</div>
                    <div className="email-input__wrapper">
                        {emailSuccess ? 
                        <div className="code-wrapper"> 
                            <ReactInputVerificationCode length={4} placeholder={""} onCompleted={() => setCodeSuccess(true)} />
                            <Countdown date={Date.now() + 30000} renderer={renderer} precision={3} intervalDelay={0}/>
                        </div> 
                            :
                        <>
                            <div className="email-input">
                                <input type="text" class={`inputText ${emailValue.length > 0 ? "input-text-active" : " "} ${errorMessage ? "input-error" : ""}`} onChange={(e) => setEmailValue(e.target.value)}/>
                                <span class={`floating-label ${emailValue.length > 0 ? "floating-label-active" : " "} ${errorMessage ? "label-error" : ""}`}>Write e-mail</span>
                            </div>
                            <div className="error-msg">{errorMessage ? "Email is not valid" : ""}</div>                                           
                        </>
                        }
                    </div>
                </div>
                {emailSuccess ?                 
                <>
                    <button className="send-code-btn" onClick={() => validateEmail()} disabled={codeSuccess ? false : true}>
                    Submit
                    </button>
                </> 
                :
                <>
                    <button className="send-code-btn" onClick={() => validateEmail()} disabled={emailValue ? false : true}>
                    Send a code
                    </button>
                </> 
            }
            </div>
    )
}

export default VerificationPage;