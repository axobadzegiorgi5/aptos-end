import carouselImage1 from "./carouselimages/carousel1.png"
import carouselImage2 from "./carouselimages/carousel2.png"
import carouselImage3 from "./carouselimages/carousel3.png"
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import { useState } from "react";
import "./carousel.css"

const ImageCarousel = () => {

    const [sliderNumber, setSliderNumber] = useState(0)
    
    return(
        <div className="startpage-carousel">
                    <div className="carousel-overlay"></div>
        <div className="circles">
        <svg class="progress-circle "  xmlns="http://www.w3.org/2000/svg">
        <circle class={`progress-circle-prog ${sliderNumber == 0 ? 'circle-active' : ""}`} 
                cx="100" cy="100" r="74"></circle>
        </svg>	
        <svg class="progress-circle " xmlns="http://www.w3.org/2000/svg">
        <circle class={`progress-circle-prog ${sliderNumber == 1 ? 'circle-active' : ""}`} 
                cx="100" cy="100" r="74"></circle>
        </svg>	
        <svg class="progress-circle " xmlns="http://www.w3.org/2000/svg">
        <circle class={`progress-circle-prog ${sliderNumber == 2 ? 'circle-active' : ""}`} 
                cx="100" cy="100" r="74"></circle>
        </svg>	

        </div>

            <Carousel loop autoPlay interval={4000} onChange={(e) => setSliderNumber(e)} showIndicators={false} infiniteLoop stopOnHover={false} showStatus={false} showThumbs={false} animationHandler={'fade'} showArrows={false}>
                <div>
                    <img src={carouselImage1} />
                    
                </div>
                <div>
                    <img src={carouselImage2} />
            
                </div>
                <div>
                    <img src={carouselImage3} />
            
                </div>
            </Carousel>
    </div>
    )
}

export default ImageCarousel;