
import UploadIcon from "./uploadImages/upload-files.svg"
import GenerateIcon from "./uploadImages/generate-link.svg"
import "./uploadsection.css"
import { Link } from "react-router-dom";

const UploadSection = () => {

    return(
        <div className="upload-section">
            <div className="upload-header">Upload & transfer files</div>
            <Link to="/email-verification" style={{ textDecoration: 'none' }}>
            <div className="upload-files__container">
                    <div className="upload-icon"></div>
                    <p className="upload-title">Upload files</p>
                    <p className="upload-subtitle">There should go a small descriptio
                    text about what is “Upload files”</p>
                </div>
            </Link>

            <div className="upload-files__container">
                    <img className="upload-icon" src={GenerateIcon}/>
                    <p className="upload-title">Generate empty link</p>
                    <p className="upload-subtitle">There should go a small descriptio
                    text about what is “Upload files”</p>
            </div>
        </div>
    )
}

export default UploadSection;