import "./header.css"
import logo from "./headercomponents/logo.svg"
import helpIcon from "./headercomponents/help-icon.svg"
import { Link } from "react-router-dom";
const Header = () => {
    return(
        <div className="header">
            <Link to="/" className="header-logo">
            <img className = "header-logo" src={logo}/>
            </Link>
            
            <div className="header-help__wrapper">
                <div className="header-help__icon">
                    
                    <img  src={helpIcon}/>
                    
                </div>
                <p className="header-help__text">How it works</p>
            </div>
        </div>
    )
}

export default Header;