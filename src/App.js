import logo from './logo.svg';
import './App.css';
import StartPage from './components/startpage/startpage';
import Header from './components/common/header/header';
import { Route, Routes } from 'react-router-dom';
import { BrowserRouter as Router } from "react-router-dom";
import ImageCarousel from './components/startpage/carousel/carousel';
import VerificationPage from './components/verificationPage/verificationpage';

function App() {
  return (
    <div className="App">
            <Header/>
      <div className="startpage">
      <Routes>
        <Route path="/" element={<StartPage/>}>
        </Route>
        <Route path="/email-verification" element={<VerificationPage/>}>
        </Route>
      </Routes>
      <ImageCarousel/>
      </div>





    </div>
  );
}

export default App;
